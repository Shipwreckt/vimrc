" enable syntax "
syntax on

" enable line numbers "
set number

" highlight current line "
set cursorline
highlight CursorLine cterm=bold ctermbg=53 

" enable highlight search pattern "
set hlsearch

"Status bar
set laststatus=2


" enable smartcase search sensitivity "
set ignorecase
set smartcase
set smartindent

" autoindent:	autoindent in new line
set tabstop	=4
set softtabstop	=4
set shiftwidth	=4
set textwidth	=100
set expandtab
set autoindent
